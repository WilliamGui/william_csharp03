﻿using System;
using System.Collections.Generic;
using System.Text;
using William_csharp03.WeaponList;

namespace William_csharp03
{
    public class Player
    {
        public string Nama { get; set; }
        public Weapon Senjata { get; set; }

        public void ShowComment()
        {
            int NoSenjata;
            bool salah = true;
            Console.WriteLine("Halo selamat datang di game Hilda Online");
            Console.Write("Silakan Masukan Nama anda : ");
            Nama = Console.ReadLine();
            Console.WriteLine("Nama Anda dalah : " + Nama);
            Console.WriteLine("Siklakan Pilih Senjata anda dengan menulis angka :");
            Console.WriteLine("1. Panah (3000)  2. Tombak (2000) 3. Dual Pedang (5000)");
            Console.Write("Pilih No :");

            if (int.TryParse(Console.ReadLine(), out NoSenjata))
            {
                while (NoSenjata > 3)
                {
                    Console.Write("Pilih No yang tersedia :");
                    int.TryParse(Console.ReadLine(), out NoSenjata);
                }

                if (NoSenjata == 1)
                {
                    Senjata = new Panah();
                    Console.WriteLine("senjata Anda dalah : " + Senjata.Nama);
                    Console.WriteLine("Moster dengan darah 10000, Player bernama " + Nama + " mengalah kannya dengan " + 10000 / Senjata.Demage + " kali serangan");
                    Console.WriteLine("Finish");
                }
                else if (NoSenjata == 2)
                {
                    Senjata = new Tombak();
                    Console.WriteLine("senjata Anda dalah : " + Senjata.Nama);
                    Console.WriteLine("Moster dengan darah 10000, Player bernama " + Nama + " mengalah kannya dengan " + 10000 / Senjata.Demage + " kali serangan");
                    Console.WriteLine("Finish");
                }
                else if (NoSenjata == 3)
                {
                    Senjata = new DualPedang();
                    Console.WriteLine("senjata Anda dalah : " + Senjata.Nama);
                    Console.WriteLine("Moster dengan darah 10000, Player bernama " + Nama + " mengalah kannya dengan " + 10000 / Senjata.Demage + " kali serangan");
                    Console.WriteLine("Finish");
                }
            }
            else
            {
                while (salah)
                {
                    Console.Write("Pilih No yang tersedia :");
                    salah = int.TryParse(Console.ReadLine(), out NoSenjata) == false;
                }
                if (NoSenjata == 1)
                {
                    Senjata = new Panah();
                    Console.WriteLine("senjata Anda dalah : " + Senjata.Nama);
                    Console.WriteLine("Moster dengan darah 10000, Player bernama " + Nama + " mengalah kannya dengan " + 10000 / Senjata.Demage + " kali serangan");
                    Console.WriteLine("Finish");
                }
                else if (NoSenjata == 2)
                {
                    Senjata = new Tombak();
                    Console.WriteLine("senjata Anda dalah : " + Senjata.Nama);
                    Console.WriteLine("Moster dengan darah 10000, Player bernama " + Nama + " mengalah kannya dengan " + 10000 / Senjata.Demage + " kali serangan");
                    Console.WriteLine("Finish");
                }
                else if (NoSenjata == 3)
                {
                    Senjata = new DualPedang();
                    Console.WriteLine("senjata Anda dalah : " + Senjata.Nama);
                    Console.WriteLine("Moster dengan darah 10000, Player bernama " + Nama + " mengalah kannya dengan " + 10000 / Senjata.Demage + " kali serangan");
                    Console.WriteLine("Finish");
                }
            }
        }
    }
}